// let express = require('express');
// let app = express();
// let port = process.env.PORT || 3000;

// app.listen(port);

// console.log('RESTful API server started on: ' + port);
const Express = require("express");
const Mongoose = require("mongoose");
const BodyParser = require("body-parser");
Mongoose.connect("mongodb://localhost/task", {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});
const PersonModel = Mongoose.model("person", {
    firstname: {
        type: String
    },
    status: {
        type: Boolean,
        default: false
    },
    order: {
        type: Number,
        default: 0
    },
    date: {
        type: Date,
        default: Date.now
    }

});
var app = Express();
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
// Define REST API
app.post("/person", async (request, response) => {
    console.log("chạy hàm thứ 1");

    try {
        var person = new PersonModel(request.body);
        console.log("hi", person);

        var result = await person.save();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }

});
app.get("/people", async (request, response) => {
    try {
        var result = await PersonModel.find().exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});
app.put("/person/:id", async (req, response) => {
    PersonModel.findOneAndUpdate(req.params.id, { new: true }, function (err, data) {
        if (err) return send(err)
        let dataa = {
            status: !data.status
        }
        PersonModel.findOneAndUpdate(req.params.id, dataa, { new: true }, function (err, value) {
            if (err) return send(err)
            response.send(value)
        })
    })
});
app.put("/lookme", async (request, response) => {
    var order_list = request.body
    //console.log("heeek",order_list);

    for (let i = 0; i < order_list.length; i++) {


        await PersonModel.findOneAndUpdate({ _id: order_list[i]._id }, { order: order_list[i].order }, { new: true })
        //console.log("hhhrhrhrh", { _id: request.order_list[i]._id }, { order: request.order_list[i].order });
        console.log("CHAY HAM NAY");
    }
    response.send("ddax suwa")

});

app.delete("/person/:id", async (request, response) => {
    console.log(request);

    PersonModel.findByIdAndDelete(request.params.id, function (err, data) {
        if (err) return response.send(err)
        response.send(data)
    }
    )
});
app.listen(3001, () => {
    console.log("Listening at :3000...");
});
console.log("Bài tập 1");

var mang = [1, 9, 3, 7, 5, 8]
var max = mang[0]
var min = mang[0]

// hàm sort() này giúp sắp xếp các phần tử theo giá trị tăng dần
console.log("Đã sắp xếp theo giá trị tăng dần", mang.sort());

for (i = 0; i < mang.length; i++) {
    if (mang[i] > max) {
        max = mang[i]
    }
    else if (mang[i] <= min) {
        min = mang[i]
    }
}
console.log(" Giá trị lớn nhất của mảng", max);
console.log(" Giá trị nhỏ nhất của mảng", min);
// tìm số không phải số nguyên tố
var soNT=[]
var soKPNT=[]
// for (let index = 1; index <=100; index++) {
//     mang[index-1]=index;
    
// }
for(i=0;i<mang.length;i++){
    if (mang[i]==1||mang[i]==2) {
        soNT.push(mang[i])
    }
    for(j=2;j<mang[i];j++){
        if(mang[i]%j==0){
            soKPNT.push(mang[i])
            console.log("d",j);
            
            break
            break
        }
 
        else if(j==mang[i]-1){
            soNT.push(mang[i])
        }
    }
}
console.log("số nguyên tố",soNT);
console.log("số không phải nguyên tố",soKPNT);

// bài tập hợp nhất hai mảng và loại bỏ phần tử trung lặp
//từ kết quả trên tạo ra hai mảng  lẻ- chẵn
console.log("Bài tập 2");

var mangphu1 = [0, 1, 3, 9, 8, 5, 7]
var mangphu2 = [1, 2, 3, 4, 5, 6, 7]
var mangchinh = mangphu1.concat(mangphu2)
var unique = [...new Set(mangchinh)]
console.log("Hợp nhất hai mảng và loại bỏ phần tử trùng lặp", unique);
var mang0 = []
var mangnew1 = []
var mangnew2 = []
for (i = 0; i < unique.length; i++) {
    if (unique[i] % 2 == 0 && unique[i] != 0) {
        mangnew1.push(unique[i])
    }
    else if (unique[i] == 0) {
        mang0.splice(unique[i])
    }
    else {
        mangnew2.push(unique[i])
    }
}
console.log("tạo ra 2 mảng chẵn và lẻ");


console.log("mảng chẵn", mangnew1);
console.log("mảng lẻ", mangnew2);

var array1 = [0, 1, 3, 9, 8, 5, 7]
var array2 = [1, 2, 3, 4, 5, 6, 7]
for(i=0;i<array1.length;i++){
    array2.push(array1[i])
}
console.log("mảng mới",array2);
var array2=[ 1, 2, 3, 4, 5, 6,
    7, 0, 1, 3, 9, 8,
    5, 7]
var mangnew=[]
for(i=0;i<array2.length;i++){
 if(array2[i]-1==array2[i]){
    mangnew.push(array2[i])
 }
}
console.log("mảng mới nhất",mangnew);
